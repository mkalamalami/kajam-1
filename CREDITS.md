* "Inversionz" font by Darrell Flood (Donationware)
  https://www.dafont.com/fr/font-comment.php?file=inversionz
* "Metal Plate, Grey, Dirty" by Georges "TRaK" Grondin (CC-BY-SA 3.0)
  https://opengameart.org/content/metal-plate-grey-dirty
* "Rusted Metal Texture Pack" by p0ss (CC-BY 3.0)
  https://opengameart.org/content/rusted-metal-texture-pack*
