﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ExitComponent : MonoBehaviour {

    public DoorComponent door;

    private bool activated = false;

    public AudioSource exitMusic;

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (!activated && collider.gameObject.CompareTag("Player"))
        {
            door.Close();
            activated = true;
            CameraComponent camera = FindObjectOfType<CameraComponent>();
            camera.ZoomIn();
            CanvasComponent canvas = FindObjectOfType<CanvasComponent>();
            canvas.baseColor = Color.white;
            canvas.FadeOut(0.01f);
            StartCoroutine(LoadEnd());
        }
    }

    IEnumerator LoadEnd()
    {
        yield return new WaitForSeconds(6f);
        SceneManager.LoadScene("Ending");
    }

    public void StartThemeMusic()
    {
        if (!exitMusic.isPlaying)
        {
            exitMusic.Play();
        }
        DontDestroyOnLoad(exitMusic.gameObject);
    }

    public void StopThemeMusic()
    {
        exitMusic.Stop();
    }
}
