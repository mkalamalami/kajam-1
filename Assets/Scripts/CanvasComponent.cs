﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasComponent : MonoBehaviour {

    public Image fade;

    private float targetFadeAlpha = 0f;

    private float speed = 0.008f; // initial fade in

    public Color baseColor = Color.black;

    void Start()
    {
        fade.color = GetAlpha(1f);
        fade.gameObject.SetActive(true);
    } 

    void Update()
    {
        if (fade.color.a != targetFadeAlpha)
        {
            fade.color = GetAlpha(fade.color.a + (targetFadeAlpha - fade.color.a) * speed);
        }
    }

    public void FadeOut(float speed)
    {
        fade.color = GetAlpha(0f);
        targetFadeAlpha = 1f;
        this.speed = speed;
    }

    public void FadeIn(float speed)
    {
        fade.color = GetAlpha(1f);
        targetFadeAlpha = 0f;
        this.speed = speed;
    }

    private Color GetAlpha(float alpha)
    {
        return new Color(baseColor.r, baseColor.g, baseColor.b, alpha);
    }

}
