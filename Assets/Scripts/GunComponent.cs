﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunComponent : MonoBehaviour {

    public GameObject bulletPrefab;

    public AudioClip shotClip;

    public GunLayouts gunLayouts;

    private GunFlashComponent flash;

    private AudioSource source;

    private float delay = 0;

    void Start () {
        flash = transform.Find("Flash").GetComponent<GunFlashComponent>();
        source = GetComponent<AudioSource>();
	}
    
    public void Recalibrate(int index, int total, float cooldown)
    {
        transform.localPosition = gunLayouts.GetOffset(index, total);
        delay = cooldown * index / total;
    }

    public void Shoot(Vector2 direction)
    {
        StartCoroutine(DoShoot(direction));
    }

    IEnumerator DoShoot(Vector2 direction)
    {
        yield return new WaitForSeconds(delay);

        GameObject bullet = Instantiate(bulletPrefab, null);
        bullet.transform.position = flash.transform.position;
        BulletComponent bulletComponent = bullet.GetComponent<BulletComponent>();
        bulletComponent.direction = direction;
        if (bulletComponent.direction.x != 1) // XXX Why?
        {
            bullet.transform.Rotate(0, 0, Vector2.Angle(Vector2.up, direction));
        }
        else
        {
            bullet.transform.Rotate(0, 0, -90);
        }
        
        FindObjectOfType<CameraComponent>().Shake(0.1f);
        source.clip = shotClip;
        source.pitch = UnityEngine.Random.Range(0.9f, 1.1f);
        source.volume = UnityEngine.Random.Range(0.8f, 1f);
        source.Play();
        flash.Flash();
    }
	
	void Update () {
		
	}
}
