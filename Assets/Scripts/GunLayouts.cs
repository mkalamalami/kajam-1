﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GunLayouts", menuName = "Gun Layouts", order = 3)]
public class GunLayouts : ScriptableObject
{

    public List<GunLayout> layouts;

    public Vector3 GetOffset(int index, int total)
    {
        if (total > layouts.Count || index >= layouts[total - 1].offsetsX.Count || index >= layouts[total - 1].offsetsY.Count)
        {
            Debug.Log("Invalid gun: " + index + "/" + total);
            return Vector3.zero;
        }
        return new Vector3(layouts[total - 1].offsetsX[index], layouts[total - 1].offsetsY[index]);
    }

    public int GetMaxGuns()
    {
        return layouts.Count;
    }
    
}
