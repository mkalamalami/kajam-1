﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SplashComponent : MonoBehaviour {

    public List<SpriteRenderer> sprites;

	void Start () {
        StartCoroutine(AnimateSplash());
	}

    void Update()
    {
        if (Input.GetButtonDown("Exit") || Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Return))
        {
            SceneManager.LoadScene("Game");
        }
    }

	IEnumerator AnimateSplash() {
        yield return new WaitForSeconds(11f);
        for (int i = 0; i < sprites.Count * 10; i++)
        {
            SpriteRenderer s = null;
            int attempts = 0;
            while ((s == null || s.color.a == 0) && attempts++ < 10)
            {
                s = sprites[Random.Range(0, sprites.Count)];
            }
            s.color = new Color(1, 1, 1, s.color.a - 0.2f);

            GetComponent<AudioSource>().volume -= 1f / (sprites.Count * 6);
            yield return new WaitForSeconds(0.1f);
        }
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene("Game");
	}

}
