﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplashLetterComponent : MonoBehaviour {

    private const float SPEED = 1.8f;

    public float initialOffsetX = 0;
        
    public float initialOffsetY = 0;

    private Vector3 targetPosition;

    private float offsetX = 0;
        
    private float offsetY = 0;

	void Start () {
        targetPosition = transform.position;
        offsetX = initialOffsetX;
        offsetY = initialOffsetY;
        transform.position += new Vector3(initialOffsetX, initialOffsetY);
	}
	
	void Update () {
        if (offsetX != 0 || offsetY != 0)
        {
            offsetX -= Mathf.Sign(offsetX) * SPEED * Time.deltaTime;
            offsetY -= Mathf.Sign(offsetY) * SPEED * Time.deltaTime;
        }
        
        transform.position = targetPosition + new Vector3(offsetX, offsetY);
    }
}
