﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorComponent : MonoBehaviour
{

    private const float LEFT_OPEN = -1.025f;

    private const float LEFT_CLOSED = -0.365f;

    private const float RIGHT_OPEN = 1.025f;

    private const float RIGHT_CLOSED = 0.365f;

    public Transform left;

    public Transform right;

    public AudioClip open;

    public AudioClip close;

    private float targetLeft = LEFT_OPEN;

    private float targetRight = RIGHT_OPEN;

    private bool seald = false;

    private AudioSource source;

    void Start()
    {
        source = GetComponent<AudioSource>();
        source.pitch = Random.Range(0.8f, 1.2f);
    }

    public void Open()
    {
        if (!seald) {
            targetLeft = LEFT_OPEN;
            targetRight = RIGHT_OPEN;
            StartCoroutine(Animate(open));
        }
    }

    public void Close()
    {
        targetLeft = LEFT_CLOSED;
        targetRight = RIGHT_CLOSED;
        StartCoroutine(Animate(close));
    }

    public void Seal()
    {
        seald = true;
        Close();
    }

    private IEnumerator Animate(AudioClip sound)
    {
        if (!IsAnimationComplete())
        {
            if (source)
            {
                source.clip = sound;
                source.time = Random.Range(0f, 0.4f);
                source.Play();
            }
            while (!IsAnimationComplete())
            {
                SetX(left, (targetLeft + left.localPosition.x * 4) / 5);
                SetX(right, (targetRight + right.localPosition.x * 4) / 5);
                yield return new WaitForSecondsRealtime(0.015f);
            }
        }
        SetX(left, targetLeft);
        SetX(right, targetRight);
    }

    private bool IsAnimationComplete()
    {
        return Mathf.Abs(left.localPosition.x - targetLeft) < 0.02f
            && Mathf.Abs(right.localPosition.x - targetRight) < 0.02f;
    }

    private void SetX(Transform t, float x)
    {
        t.localPosition = new Vector3(x, t.localPosition.y, t.localPosition.z);
    }

}