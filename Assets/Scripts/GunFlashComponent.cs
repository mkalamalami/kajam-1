﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunFlashComponent : MonoBehaviour {

    public List<Sprite> flashSprites;

    private new Light light;

    private SpriteRenderer sprite;

    void Start()
    {
        light = GetComponent<Light>();
        sprite = GetComponent<SpriteRenderer>();

        light.enabled = false;
        sprite.enabled = false;
    }

    public void Flash()
    {
        StartCoroutine(DoFlash());
    }

    public IEnumerator DoFlash() {
        sprite.sprite = flashSprites[Random.Range(0, flashSprites.Count)];

        sprite.enabled = true;
        light.enabled = true;

        yield return new WaitForSeconds(0.1f);

        sprite.enabled = false;
        light.enabled = false;
    }

}
