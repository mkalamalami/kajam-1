﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemComponent : MonoBehaviour {

    public enum Item
    {
        WEAPON,
        HEALTH
    }

    public Item item;

    public LevelComponent level;

    void Update()
    {
        transform.Rotate(0, 0, -60 * Time.deltaTime);
    }

    public void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Player")
        {
            GetComponent<AudioSource>().Play();
            collider.GetComponent<PlayerComponent>().GiveItem(item);
            if (level != null)
            {
                level.MarkObjectDead(gameObject);
            }
            StartCoroutine(DoDestroy());
        }
    }

    private IEnumerator DoDestroy()
    {
        Destroy(GetComponent<SpriteRenderer>());
        Destroy(GetComponent<BoxCollider2D>());
        yield return new WaitForSeconds(1f);
        Destroy(gameObject);
    }
}
