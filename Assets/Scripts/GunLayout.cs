﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GunLayout", menuName = "Gun Layout", order = 4)]
public class GunLayout : ScriptableObject {
    
    public List<float> offsetsX;

    public List<float> offsetsY;

}
