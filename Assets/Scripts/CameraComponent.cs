﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraComponent : MonoBehaviour {

    private const int SMOOTHNESS = 20;

    private const float LEVEL_BORDER = 6.5f;

    private const float SHAKE_STRENGTH = 0.2f;

    public Transform target;

    private Transform defaultTarget;

    private new Camera camera;

    private float shakeStrength = 0;

    private float defaultSize = 4.2f;

    private static float timeScale = 1;

    private static float timeScaleDate = 0;

    void Start()
    {
        defaultTarget = target;
        camera = GetComponent<Camera>();

        // Make sure the whole level is always visible whatever the resolution is
        Vector3 rightPos = Camera.main.ViewportToWorldPoint(new Vector2(0.97f, 0));
        if (rightPos.x < LEVEL_BORDER)
        {
            defaultSize += LEVEL_BORDER - rightPos.x;
        }

        // Don't animate during layout debug
        LevelGeneratorComponent levelGen = FindObjectOfType<LevelGeneratorComponent>();
        if (levelGen == null || !FindObjectOfType<LevelGeneratorComponent>().db.originLayout.HasSpawner())
        {
            ZoomOut();
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

    void LateUpdate()
    {
        Time.timeScale = timeScale;
        if (Time.realtimeSinceStartup - timeScaleDate > 0.02f)
        {
            timeScale = 1f;
        }
    }

    public static void FreezeTimeForAMoment()
    {
        timeScale = 0.3f;
        timeScaleDate = Time.realtimeSinceStartup;
    }

    void Update()
    {
        Vector3 newPos = (transform.position * SMOOTHNESS + target.position) / (SMOOTHNESS + 1);
        transform.position = new Vector3(newPos.x, newPos.y, transform.position.z);
        if (shakeStrength > 0)
        {
            transform.position += SHAKE_STRENGTH * new Vector3(Random.Range(-shakeStrength, shakeStrength), Random.Range(-shakeStrength, shakeStrength));
            shakeStrength -= Time.deltaTime;
        }
    }

    public void Shake(float strength)
    {
        shakeStrength = Mathf.Max(shakeStrength, strength);
    }

    public void Attach(Transform t)
    {
        target = t;
    }

    public void Detach()
    {
        target = defaultTarget;
    }

    public void ZoomIn()
    {
        StartCoroutine(Zoom(defaultSize, defaultSize / 2));
    }

    public void ZoomOut()
    {
        StartCoroutine(Zoom(defaultSize / 2, defaultSize));
    }

    public IEnumerator Zoom(float from, float to) { 
        camera.orthographicSize = from;
        while (Mathf.Abs(camera.orthographicSize - to) > 0.001f)
        {
            camera.orthographicSize += (to - camera.orthographicSize) * 0.01f;
            yield return new WaitForSecondsRealtime(0.018f);
        }
        camera.orthographicSize = to;
    }

}
