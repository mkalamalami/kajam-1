﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelComponent : MonoBehaviour
{

    public List<DoorComponent> doors = new List<DoorComponent>();

    public List<Color> gelColors;

    public SpriteRenderer gelSprite;

    public TextMesh hintTextMesh;

    public new Light light;

    public int i = 0;

    public int j = 0;

    private List<GameObject> aliveObjects = new List<GameObject>();

    private bool activated = false;

    private bool ended = false;

    public int GetHash()
    {
        return GetHashStatic(i, j);
    }

    public static int GetHashStatic(int i, int j)
    {
        return (FindObjectOfType<LevelGeneratorComponent>().worldSize * 2 + 1) * j + i;
    }

    public void Randomize()
    {
        SetGelColor(gelColors[Random.Range(0, gelColors.Count)]);

        AudioSource audio = GetComponent<AudioSource>();
        audio.pitch = Random.Range(0.6f, 1.2f);
        audio.pitch *= Random.Range(0, 2) * 2 - 1;
        audio.time = Random.Range(0, audio.clip.length);
    }

    public void AddAliveObject(GameObject obj)
    {
        aliveObjects.Add(obj);
    }
    
    public void MarkObjectDead(GameObject obj)
    {
        aliveObjects.Remove(obj);
        TestLevelEnd();
    }
    
    public bool HasAliveObjects()
    {
        return aliveObjects.Count > 0;
    }

    public void SetGelColor(Color color)
    {
        gelSprite.color = color;
        light.color = color;
    }

    public void SetHint(string text)
    {
        hintTextMesh.gameObject.SetActive(true);
        hintTextMesh.text = text;
    }

    public void SealDoors(bool top, bool right, bool bottom, bool left)
    {
        bool[] seals = new bool[] { top, right, bottom, left };
        string[] names = new string[] { "N", "E", "S", "W" };
        for (int i = 0; i < 4; i++)
        {
            if (seals[i])
            {
                doors[i].Seal();
                Destroy(transform.Find("Corridor" + names[i]).gameObject);
            }
        }
    }

    void TestLevelEnd()
    {
        if (!ended && aliveObjects.Count == 0)
        {
            foreach (DoorComponent door in doors)
            {
                door.Open();
            }
            FindObjectOfType<CameraComponent>().Detach();
            ended = true;
        }
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.CompareTag("Player"))
        {
            FindObjectOfType<LevelGeneratorComponent>().OnRoomEnter(this.i, this.j);
            collider.GetComponent<PlayerComponent>().OnRoomEnter();
            if (!activated && (aliveObjects.Count > 1 || 
                aliveObjects.Count == 1 && aliveObjects[0].GetComponent<SpawnerComponent>() != null))
            {
                foreach (GameObject obj in aliveObjects)
                {
                    if (obj.GetComponent<SpawnerComponent>() != null) { 
                        obj.GetComponent<SpawnerComponent>().Deploy();
                    }
                }
                foreach (DoorComponent door in doors)
                {
                    door.Close();
                }
                FindObjectOfType<CameraComponent>().Attach(transform);
                activated = true;
            }

            ExitComponent exit = FindObjectOfType<ExitComponent>();
            if (exit != null)
            {
                if (MazeGenerator.HINT_ZERO.Equals(hintTextMesh.text))
                {
                    exit.StartThemeMusic();
                }
                else
                {
                    exit.StopThemeMusic();
                }
            }
        }
    }

    void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.gameObject.CompareTag("Player"))
        {
            collider.GetComponent<PlayerComponent>().OnRoomExit();
        }
    }

}