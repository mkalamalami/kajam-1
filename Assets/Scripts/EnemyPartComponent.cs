﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPartComponent : MonoBehaviour {

    public List<Sprite> parts;

    private const float SPEED = 4f;

    private const float ROTATION_SPEED = 3f;

    private const float LIFESPAN = 1f;

    private float rotation = 1f;

    private float lifespan = LIFESPAN;

    private Vector2 speed;

    private float rotationSpeed;

    private Rigidbody2D r;

    private AudioSource source;

    private bool soundPlayed = false;

    void Start()
    {
        GetComponent<SpriteRenderer>().sprite = parts[Random.Range(0, parts.Count)];
        r = GetComponent<Rigidbody2D>();
        transform.localScale *= Random.Range(0.4f, 0.7f);
        transform.Rotate(0, 0, Random.Range(0f, 360f));
        source = GetComponent<AudioSource>();
    }

    public void Init(Vector3 parentPosition)
    {
        transform.position = parentPosition;

        r = GetComponent<Rigidbody2D>();
        r.velocity = speed = new Vector2(Random.Range(-1f, 1f), Random.Range(-1f, 1f)).normalized
                * Random.Range(0.7f, 1.3f) * SPEED;
        r.angularVelocity = rotationSpeed = Random.Range(-ROTATION_SPEED, ROTATION_SPEED);
    }
    
	void Update () {
        lifespan -= Time.deltaTime;
        
        if (lifespan < 0)
        {
            Destroy(gameObject);
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        GameObject collided = collision.gameObject;
        if (collided.CompareTag("Wall") && !soundPlayed)
        {
            soundPlayed = true;
            source.pitch = UnityEngine.Random.Range(0.6f, 1.4f);
            source.Play();
        }
    }

}
