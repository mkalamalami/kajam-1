﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Enemy", menuName = "Enemy Info", order = 3)]
public class EnemyInfo : ScriptableObject
{
    public GameObject prefab;

    public Sprite bodySprite;

    public Sprite legSprite;

    public int health;

    public float speed;

    public float scale;

}
