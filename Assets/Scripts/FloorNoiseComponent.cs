﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorNoiseComponent : MonoBehaviour {

    private Vector2 direction;

    private Material material;

    void Start()
    {
        material = GetComponent<SpriteRenderer>().material;
        direction = Quaternion.Euler(0, 0, Random.Range(0, 360)) * Vector2.up * 0.03f;
        Update();
    }
	
	void Update () {
        material.mainTextureOffset += direction * Time.deltaTime;
    }
}
