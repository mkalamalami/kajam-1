﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbstractComponent : MonoBehaviour {

    protected IEnumerator Animate(Vector3 targetScale, float targetAlpha = 1, int fastness = 4)
    {
        SpriteRenderer sprite = GetComponent<SpriteRenderer>();

        while (Vector3.Distance(transform.localScale, targetScale) > 0.02f)
        {
            transform.localScale = (transform.localScale * fastness + targetScale) / (fastness + 1);
            if (sprite != null)
            {
                sprite.color = new Color(sprite.color.r, sprite.color.g, sprite.color.b,
                    (sprite.color.a * fastness + targetAlpha) / (fastness + 1));
            }
            yield return new WaitForSecondsRealtime(0.018f);
        }
        transform.localScale = targetScale;
        sprite.color = new Color(sprite.color.r, sprite.color.g, sprite.color.b, targetAlpha);

    }

    protected void Flash(SpriteRenderer sprite, Color color, float amount)
    {
        sprite.material.SetColor("_FlashColor", color);
        sprite.material.SetFloat("_FlashAmount", amount);
    }

    protected void Play(AudioSource source, AudioClip clip, float pitchRange, float volumeRange)
    {
        source.clip = clip;
        source.pitch = UnityEngine.Random.Range(1 - pitchRange, 1 + pitchRange);
        source.volume = UnityEngine.Random.Range(1 - volumeRange, 1 + volumeRange);
        source.loop = false;
        source.Play();
    }

}
