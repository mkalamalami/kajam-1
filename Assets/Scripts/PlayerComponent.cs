﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class PlayerComponent : AbstractComponent {

    private const float ACCELERATION = 100f;

    private const float RECOIL_FORCE = 120f;

    private const float CAMERA_AHEAD = 0.2f;

    private const float INVINCIBILITY_DURATION = 1.0f;

    private const float INITIAL_GUN_COOLDOWN = 0.13f;

    private const float INITIAL_GUN_COOLDOWN_PER_ADDITIONAL_GUN = 0.08f;

    private const int MAX_HEALTH = 4;

    public Animator animator;

    public Transform cameraTarget;

    public List<GameObject> wounds;

    public List<GameObject> blood;

    public ParticleSystem bloodSplash;

    public GameObject gunPrefab;

    public AudioSource walkSource;

    public AudioClip roomWalkClip;

    public AudioClip corridorWalkClip;

    public AudioClip hitClip;

    public AudioClip deathClip;

    public AudioClip shotClip;

    private AudioSource source;
    
    private Rigidbody2D r;

    private List<GunComponent> guns = new List<GunComponent>();

    private Vector3 gunDirection = Vector3.up;

    private float gunCooldownBase = INITIAL_GUN_COOLDOWN;

    private float gunCooldownPerAdditionalGun = INITIAL_GUN_COOLDOWN_PER_ADDITIONAL_GUN;

    private float gunHeat = 0;

    private float health = 4;

    private float invincible = 0f;

    public bool isDead = false;

    private bool hasWeapon = false;

    void Start () {
        r = GetComponent<Rigidbody2D>();
        source = GetComponent<AudioSource>();
        RefreshWounds();
    }

    float GetTotalCooldown()
    {
        return gunCooldownBase + gunCooldownPerAdditionalGun * (guns.Count - 1);
    }

    public void GiveItem(ItemComponent.Item item)
    {
        switch (item)
        {
            case ItemComponent.Item.WEAPON:
                this.hasWeapon = true;
                animator.SetBool("weapon", hasWeapon);

                if (guns.Count == 0 || guns.Count < guns[0].gunLayouts.GetMaxGuns())
                {
                    GameObject newGun = Instantiate(gunPrefab, transform);
                    newGun.transform.localScale = Vector3.one * (1f / transform.localScale.x);
                    guns.Add(newGun.GetComponent<GunComponent>());

                    int i = 0;
                    foreach (GunComponent gun in guns)
                    {
                        gun.Recalibrate(i++, guns.Count, GetTotalCooldown());
                    }
                } else
                {
                    gunCooldownPerAdditionalGun = Mathf.Max(0, gunCooldownPerAdditionalGun - INITIAL_GUN_COOLDOWN_PER_ADDITIONAL_GUN / 4);
                }

                break;
            case ItemComponent.Item.HEALTH:
                this.health = Mathf.Min(MAX_HEALTH, this.health + 1);
                RefreshWounds();
                break;
        }
    }
	
	void Update () {
        if (!isDead)
        {
            float h = Input.GetAxisRaw("Movement Left-Right");
            float v = Input.GetAxisRaw("Movement Up-Down");
            Vector2 movement = new Vector2(h, v);

            float sh = Input.GetAxisRaw("Shoot Left-Right");
            float sv = Input.GetAxisRaw("Shoot Up-Down");
            Vector2 shoot = new Vector2(sh, sv);

            // Movement
            r.velocity += movement * Time.deltaTime * ACCELERATION;
            animator.SetFloat("speed", movement.magnitude);
            walkSource.volume = Mathf.Min(0.8f, movement.magnitude * r.velocity.magnitude / 5);
            if (walkSource.volume > 0 && !walkSource.isPlaying)
            {
                walkSource.pitch = UnityEngine.Random.Range(0.8f, 1.2f);
                walkSource.volume = UnityEngine.Random.Range(walkSource.volume * 0.7f, walkSource.volume);
                walkSource.Play();
            }

            cameraTarget.position = transform.position + new Vector3(r.velocity.x, r.velocity.y) * CAMERA_AHEAD;

            // Rotation
            Nullable<Quaternion> newRotation = null;
            if (h != 0 && v == 0 && sh == 0 && sv == 0 || (sh != 0 && sv == 0 && hasWeapon))
            {
                float val = (sh != 0 && hasWeapon) ? sh : h;
                gunDirection = new Vector3(val, 0).normalized;
                newRotation = Quaternion.Euler(0, 0, (val < 0) ? 90 : -90);
            }
            else if (h == 0 && v != 0 && sh == 0 && sv == 0 || (sh == 0 && sv != 0 && hasWeapon))
            {
                float val = (sv != 0 && hasWeapon) ? sv : v;
                gunDirection = new Vector3(0, val).normalized;
                newRotation = Quaternion.Euler(0, 0, (val < 0) ? 180 : 0);
            }
            if (newRotation.HasValue && !transform.rotation.eulerAngles.Equals(newRotation.Value.eulerAngles))
            {
                transform.rotation = newRotation.Value;
                gunHeat = 0;
            }

            // Weapon
            if (hasWeapon)
            {
                if ((shoot.magnitude > 0 || Input.GetButton("Shoot Manual")) && gunHeat <= 0)
                {
                    foreach (GunComponent gun in guns)
                    {
                        gun.Shoot(gunDirection);
                    }
                    gunHeat = GetTotalCooldown();
                    r.AddForce(- RECOIL_FORCE * gunDirection);
                }
                else if (gunHeat > 0)
                {
                    gunHeat -= Time.deltaTime;
                }
            }

            // Invincibility
            if (invincible > 0)
            {
                invincible -= Time.deltaTime;
            }
        } else
        {
            animator.SetTrigger("death");
        }
    }

    private List<SpriteRenderer> GetSprites() {
        Transform spritesRoot = transform.Find("Animator");
        List<SpriteRenderer> sprites = new List<SpriteRenderer>();
        for (int i = 0; i < spritesRoot.childCount; i++)
        {
            SpriteRenderer child = spritesRoot.GetChild(i).GetComponent<SpriteRenderer>();
            sprites.Add(child);
        }
        return sprites;
    }

    public void OnRoomEnter()
    {
        walkSource.clip = roomWalkClip;
        walkSource.Play();
    }


    public void OnRoomExit()
    {
        walkSource.clip = corridorWalkClip;
        walkSource.Play();
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        GameObject collided = collision.gameObject;
        if (collided.CompareTag("Enemy"))
        {
            Hit();
        }
    }

    private void RefreshWounds()
    {
        for (int i = 0; i < wounds.Count; i++)
        {
            wounds[i].SetActive((wounds.Count - health) >= i);
        }
    }

    public void Hit()
    {
        if (invincible <= 0)
        {
            invincible = INVINCIBILITY_DURATION;
            health--;
            RefreshWounds();

            if (health <= 0)
            {
                if (!isDead)
                {
                    isDead = true;
                    StartCoroutine(DoKill());
                }
            }
            else
            {
                StartCoroutine(DoHit());
            }
        }

    }

    IEnumerator DoHit()
    {
        source.clip = hitClip;
        source.Play();
        FindObjectOfType<CameraComponent>().Shake(0.8f);
        Instantiate(blood[UnityEngine.Random.Range(0, blood.Count)], transform.position,
            Quaternion.Euler(0, 0, UnityEngine.Random.Range(0, 360)), null);
        bloodSplash.Play();

        List<SpriteRenderer> sprites = GetSprites();
        foreach (SpriteRenderer sprite in sprites)
        {
            Flash(sprite, Color.red, 1f);
        }
        yield return new WaitForSeconds(0.2f);

        const int BLINKS = 6;
        float blinkPeriod = invincible / BLINKS / 2;
        for (int i = 0; i < BLINKS; i++)
        {
            foreach (SpriteRenderer sprite in sprites)
            {
                Flash(sprite, Color.white, 0.5f);
            }
            yield return new WaitForSeconds(blinkPeriod);
            foreach (SpriteRenderer sprite in sprites)
            {
                Flash(sprite, Color.white, 0f);
            }
            yield return new WaitForSeconds(blinkPeriod);
        }
    }

    IEnumerator DoKill()
    {
        Time.timeScale = 0.2f;
        source.clip = deathClip;
        source.Play();
        Destroy(GetComponent<BoxCollider2D>());
        Destroy(GetComponent<Rigidbody2D>());
        foreach (GunComponent gun in guns)
        {
            Destroy(gun.GetComponent<SpriteRenderer>());
        }
        CameraComponent camera = FindObjectOfType<CameraComponent>();
        camera.Shake(0.7f);
        camera.ZoomIn();

        List<SpriteRenderer> sprites = GetSprites();
        foreach (SpriteRenderer sprite in sprites)
        {
            Flash(sprite, Color.red, 1f);
        }
        yield return new WaitForSeconds(0.1f);
        
        foreach (SpriteRenderer sprite in sprites)
        {
            Flash(sprite, Color.red, 0f);
        }
        yield return new WaitForSeconds(0.6f);

        FindObjectOfType<CanvasComponent>().FadeOut(0.1f);
        yield return new WaitForSeconds(0.6f);
        Time.timeScale = 1f;
        SceneManager.LoadScene("Game");
    }
}
