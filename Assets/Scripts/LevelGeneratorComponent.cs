﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGeneratorComponent : MonoBehaviour {

    public const int LEVEL_WIDTH = 18;

    public const int LEVEL_HEIGHT = 13;

    private const float END_OFFSET_X = 15.5f;

    private const float END_OFFSET_Y = 13;

    private const float SPAWNER_OFFSET = 2.8f;
    
    public GameObject levelPrefab;

    public GameObject spawnerPrefab;

    public GameObject exit;

    public LevelLayout emptyRoom;

    public LevelLayout endingRoom;

    [Header("Debug settings")]

    public LevelLayoutDatabase db;

    public bool disableGeneration = false;

    public int worldSize = 4;

    private MazeGenerator.Maze maze;

    private HashSet<int> visitedLevelHashes = new HashSet<int>();

    private Dictionary<int, LevelComponent> instanciatedLevels = new Dictionary<int, LevelComponent>();

    void Start() {
        LevelComponent originLevel = FindObjectOfType<LevelComponent>();
        CreateLevelContents(originLevel, db.originLayout);

        // Debug weapons
        if (db.originLayout.HasSpawner())
        {
            int weaponCount = 1;
            if (db.originLayout.name.Contains("2")) weaponCount = 1;
            if (db.originLayout.name.Contains("3")) weaponCount = 2;
            for (int i = 0; i < weaponCount; i++)
            {
                FindObjectOfType<PlayerComponent>().GiveItem(ItemComponent.Item.WEAPON);
            }
        }

        maze = MazeGenerator.Generate(worldSize * 2 + 1);
        OnRoomEnter(0, 0);
        if (!disableGeneration)
        {
            exit.SetActive(false);
        }
    }

    public void OnRoomEnter(int i, int j)
    {
        if (!disableGeneration)
        {
            // Delete obsolete levels
            Dictionary<int, LevelComponent> remainingLevels = new Dictionary<int, LevelComponent>();
            foreach (LevelComponent level in instanciatedLevels.Values)
            {
                if ((level.i < i - 1 || level.i > i + 1
                    || level.j < j - 1 || level.j > j + 1)
                        && !level.HasAliveObjects())
                {
                    Destroy(level.gameObject);
                } else
                {
                    remainingLevels.Add(level.GetHash(), level);
                }
            }
            instanciatedLevels = remainingLevels;

            // Create missing levels
            for (int iNghbr = i - 1; iNghbr <= i + 1; iNghbr++)
            {
                for (int jNghbr = j - 1; jNghbr <= j + 1; jNghbr++)
                {
                    if (maze.GetHint(iNghbr + worldSize, jNghbr + worldSize) != null
                        && !(iNghbr == 0 && jNghbr == 0))
                    {
                        int neighborHash = LevelComponent.GetHashStatic(iNghbr, jNghbr);
                        if (!instanciatedLevels.ContainsKey(neighborHash))
                        {
                            LevelComponent newLevel = GenerateLevel(iNghbr, jNghbr);
                            instanciatedLevels.Add(newLevel.GetHash(), newLevel);
                        }
                    }
                }
            }

            int currentLevelHash = LevelComponent.GetHashStatic(i, j);
            if (!visitedLevelHashes.Contains(currentLevelHash))
            {
                visitedLevelHashes.Add(currentLevelHash);
            }
        }
    }

    LevelComponent GenerateLevel(int i, int j)
    {
        // Determine difficulty
        float distanceToCenterRatio = (Mathf.Abs(i) + Mathf.Abs(j)) / (2f * worldSize);
        int difficulty = (int)Mathf.Floor(3f * distanceToCenterRatio);

        // Choose layout
        string hint = maze.GetHint(i + worldSize, j + worldSize);
        LevelLayout layout;
        int hash = LevelComponent.GetHashStatic(i, j);
        if (visitedLevelHashes.Contains(hash))
        {
            layout = emptyRoom;
        }
        else if (hint == MazeGenerator.HINT_ZERO)
        {
            layout = endingRoom;
        }
        else
        {
            List<LevelLayout> layoutSource = db.layoutsLv1;
            switch (difficulty)
            {
                case 1:
                    layoutSource = db.layoutsLv2;
                    break;
                case 2:
                case 3:
                    layoutSource = db.layoutsLv3;
                    break;
            }
            layout = layoutSource[Random.Range(0, layoutSource.Count)];
        }

        // Create level
        Vector3 position = new Vector3(i * LEVEL_WIDTH, j * LEVEL_HEIGHT);
        GameObject newLevel = Instantiate(levelPrefab, transform, true);
        newLevel.name = "Level|" + i + ";" + j;
        newLevel.transform.position = position;
        newLevel.SetActive(true);
        LevelComponent level = newLevel.GetComponent<LevelComponent>();

        // Init level & exit
        level.i = i;
        level.j = j;
        level.SetHint(hint);
        level.Randomize();
        if (!hint.Equals(MazeGenerator.HINT_ZERO))
        {
            level.SealDoors(
                j == worldSize,
                i == worldSize,
                j == -worldSize,
                i == -worldSize);
        }
        else
        {
            float exitRotation = -1;
            Vector3 exitOffset = Vector3.zero;
            if (i == worldSize)
            {
                exitRotation = 90;
                exitOffset = new Vector3(END_OFFSET_X, 0);
            }
            else if (i == -worldSize)
            {
                exitRotation = -90;
                exitOffset = new Vector3(-END_OFFSET_X, 0);
            }
            else if (j == worldSize)
            {
                exitRotation = 180;
                exitOffset = new Vector3(0, END_OFFSET_Y);
            }
            else if (j == -worldSize)
            {
                exitRotation = 0;
                exitOffset = new Vector3(0, -END_OFFSET_Y);
            }
            if (exitRotation != -1)
            {
                GameObject newExit = Instantiate(exit, level.transform.position + exitOffset, Quaternion.Euler(0, 0, exitRotation), null);
                newExit.SetActive(true);
            }
        }

        CreateLevelContents(level, layout);
        return level;
    }

    void CreateLevelContents(LevelComponent level, LevelLayout layout)
    {
        if (layout != null)
        {
            // Init spawners
            if (layout.HasSpawner())
            {
                CreateSpawner(level, new Vector3(0, 0, 0), layout.center);
                CreateSpawner(level, new Vector3(0, SPAWNER_OFFSET * 0.9f, 0), layout.top);
                CreateSpawner(level, new Vector3(SPAWNER_OFFSET * 2, SPAWNER_OFFSET, 0), layout.topRight);
                CreateSpawner(level, new Vector3(SPAWNER_OFFSET * 1.8f, 0, 0), layout.right);
                CreateSpawner(level, new Vector3(SPAWNER_OFFSET * 2, -SPAWNER_OFFSET, 0), layout.botRight);
                CreateSpawner(level, new Vector3(0, -SPAWNER_OFFSET * 0.9f, 0), layout.bot);
                CreateSpawner(level, new Vector3(-SPAWNER_OFFSET * 2, -SPAWNER_OFFSET, 0), layout.botLeft);
                CreateSpawner(level, new Vector3(-SPAWNER_OFFSET * 1.8f, 0, 0), layout.left);
                CreateSpawner(level, new Vector3(-SPAWNER_OFFSET * 2, SPAWNER_OFFSET, 0), layout.topLeft);
            }

            // Init item
            Transform originItem = level.transform.Find("Item");
            if (originItem != null)
            {
                Destroy(originItem.gameObject);
            }
            if (layout.item)
            {
                layout.item.GetComponent<ItemComponent>().level = level;
                GameObject newItem = Instantiate(layout.item, level.transform);
                newItem.name = "Item";
                newItem.transform.localPosition = new Vector3(0, 1.2f);
                level.AddAliveObject(newItem);
            }
        }
    }

    void CreateSpawner(LevelComponent level, Vector3 localPosition, SpawnerInfo info)
    {
        if (info != null)
        {
            GameObject newSpawner = Instantiate(spawnerPrefab, level.transform);
            newSpawner.transform.localPosition = localPosition;
            newSpawner.GetComponent<SpawnerComponent>().Init(level, info);
            level.AddAliveObject(newSpawner);
        }
    }
}
