﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Spawner", menuName = "Spawner Info", order = 2)]
public class SpawnerInfo : ScriptableObject {

    public float delay = 0;

    public float cooldown = 0;

    public int enemyCount = 0;

    public EnemyInfo enemyInfo;

}