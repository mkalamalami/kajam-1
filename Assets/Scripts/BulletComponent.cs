﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletComponent : MonoBehaviour {

    private const float SPEED = 30f;

    public Vector3 direction = Vector3.up;

    private Rigidbody2D r;

    private bool isDead = false;

    private bool justSpawned = true;

    private float lifespan = 0.5f;

    private List<GameObject> hitTargets = new List<GameObject>();

    public ParticleSystem explosionMetal;

    void Start ()
    {
        r = GetComponent<Rigidbody2D>();
    }
	
	void Update () {
        if (!isDead)
        {
            if (justSpawned)
            {
                r.velocity = direction * SPEED;
            } else
            {
                r.velocity = r.velocity.normalized * SPEED;
            }
            lifespan -= Time.deltaTime;
            if (lifespan < 0)
            {
                Kill(false);
            }
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        GameObject collided = collision.gameObject;
        if (collided.CompareTag("Wall"))
        {
            StartCoroutine(Kill(true));
        }
        else if (collided.CompareTag("Enemy"))
        {
            EnemyComponent enemy = collided.GetComponent<EnemyComponent>();
            if (enemy != null && !hitTargets.Contains(enemy.gameObject)) {
                enemy.Hit();
            } 
        } else
        {
            PeopleComponent people = collided.GetComponent<PeopleComponent>();
            if (people != null && !hitTargets.Contains(people.gameObject))
            {
                people.Hit();
            }
            StartCoroutine(Kill(false));
        }

        if (hitTargets.Count >= 2)
        {
            StartCoroutine(Kill(false));
        }
        else
        {
            hitTargets.Add(collided);
        }
    }

    IEnumerator Kill(bool exploding)
    {
        if (!isDead)
        {
            isDead = true;
            if (exploding)
            {
                Destroy(GetComponent<SpriteRenderer>());
                Destroy(GetComponent<BoxCollider2D>());
                explosionMetal.Play();
                GetComponent<AudioSource>().Play();
                yield return new WaitForSeconds(1f);
            }
            Destroy(gameObject);
        }
    }

}
