﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndingComponent : MonoBehaviour {

    private CanvasComponent canvas;

	void Start () {
        canvas = FindObjectOfType<CanvasComponent>();
        canvas.baseColor = Color.white;
        canvas.FadeIn(0.01f);
    }
	
}
