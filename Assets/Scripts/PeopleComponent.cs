﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PeopleComponent : AbstractComponent
{

    private const float INIT_DAMOCLES = 9f;
    
    public AudioClip deathClip;

    public List<Sprite> sprites;

    public Sprite deathSprite;

    private Rigidbody2D r;

    private SpriteRenderer sprite;

    private AudioSource source;

    private LevelComponent level;

    private Transform target;

    private bool isDead = false;

    private bool isAfraid = false;

    public float damocles = INIT_DAMOCLES;

    public void Init(LevelComponent level)
    {
        this.level = level;
        level.AddAliveObject(gameObject);
        transform.localScale *= Random.Range(0.8f, 1f);
        target = FindObjectOfType<PlayerComponent>().transform;
    }

    void Start()
    {
        r = GetComponent<Rigidbody2D>();
        r.rotation = Random.Range(0, 360);
        source = GetComponent<AudioSource>();

        sprite = GetComponent<SpriteRenderer>();
        sprite.sprite = sprites[Random.Range(0, sprites.Count)];

        StartCoroutine(AnimateEntry());
    }

    IEnumerator AnimateEntry()
    {
        Color c = new Color(0.1f, 0.1f, 0.1f);
        float alpha = 1;
        while (alpha > 0)
        {
            Flash(sprite, c, alpha);
            alpha -= 0.03f;
            yield return new WaitForSecondsRealtime(0.018f);
        }
    }


    void Update()
    {
        if (!isDead)
        {
            // Rotate towards player
            float playerAngle = Vector2.SignedAngle(Vector2.up, target.position - transform.position);
            float deltaAngle = playerAngle - r.rotation;
            if (Mathf.Abs(deltaAngle) > 180)
            {
                if (playerAngle > r.rotation) r.rotation += 360;
                else r.rotation -= 360;
            }
            r.rotation += deltaAngle * 0.05f;

            if (isAfraid)
            {
                r.velocity = -(target.position - transform.position).normalized;
            }

            damocles -= Time.deltaTime;
            if (damocles < 0)
            {
                StartCoroutine(DoHit(false));
            }
        }
        else if (isDead && r != null) { 
            r.velocity *= 0.6f;
        }
    }

    public void Hit()
    {
        StartCoroutine(DoHit(true));
    }

    IEnumerator DoHit(bool byPlayer)
    {
        if (!isDead)
        {
            isDead = true;

            if (!isAfraid) {
                PeopleComponent[] peoples = FindObjectsOfType< PeopleComponent > ();
                foreach (PeopleComponent people in peoples)
                {
                    if (byPlayer)
                    {
                        people.isAfraid = true;
                        people.damocles = INIT_DAMOCLES;
                    }
                }
            }

            CameraComponent.FreezeTimeForAMoment();

            source.clip = deathClip;
            source.pitch = Random.Range(0.95f, 1.3f);
            source.Play();
            
            Flash(sprite, Color.white, 1);
            transform.Find("BloodSplash").GetComponent<ParticleSystem>().Play();
            FindObjectOfType<CameraComponent>().Shake(0.3f);

            yield return new WaitForSeconds(0.3f);

            sprite.sprite = deathSprite;
            Destroy(GetComponent<Rigidbody2D>());
            Destroy(GetComponent<CapsuleCollider2D>());
            r = null;

            yield return new WaitForSeconds(0.5f);

            if (level != null)
            {
                level.MarkObjectDead(gameObject);
            }
        }
    }
    
}
