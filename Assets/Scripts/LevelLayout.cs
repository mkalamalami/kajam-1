﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Layout", menuName = "Level Layout", order = 1)]
public class LevelLayout : ScriptableObject
{

    public SpawnerInfo center;

    public SpawnerInfo top;

    public SpawnerInfo topRight;

    public SpawnerInfo right;

    public SpawnerInfo botRight;

    public SpawnerInfo bot;

    public SpawnerInfo botLeft;

    public SpawnerInfo left;

    public SpawnerInfo topLeft;

    public GameObject item;

    public bool HasSpawner()
    {
        return topLeft != null || topRight != null || botLeft != null || botRight != null || center != null || right != null || top != null || left != null || bot != null;
    }

}
