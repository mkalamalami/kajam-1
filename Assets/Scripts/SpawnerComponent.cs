﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerComponent : MonoBehaviour {

    public List<Transform> blades;

    public int enemiesLeft = 0;

    public float cooldown = 1f;

    public float delay = 0;

    public AudioClip startClip;

    public AudioClip spawnClip;

    public AudioClip endClip;

    private float heat = 0f;

    private bool deployed = false;

    private bool animInProgress = false;

    private Vector3 deployedScale;

    private LevelComponent level;

    private AudioSource source;

    private EnemyInfo enemyInfo;

    public void Init(LevelComponent level, SpawnerInfo info)
    {
        this.level = level;
        enemiesLeft = info.enemyCount;
        delay = info.delay;
        cooldown = info.cooldown;
        enemyInfo = info.enemyInfo;
    }

    private void Play(AudioClip clip)
    {
        source.clip = clip;
        source.Play();
    }

	void Start ()
    {
        deployedScale = transform.localScale;
        transform.localScale = Vector3.zero;
        source = GetComponent<AudioSource>();
    }
	
	void FixedUpdate ()
    {
        if (deployed)
        {
            if (enemiesLeft > 0)
            {
                if (heat <= 0f)
                {
                    // Spawn enemy
                    if (enemyInfo != null)
                    {
                        GameObject newEnemy = Instantiate(enemyInfo.prefab, null);
                        newEnemy.transform.position = transform.position;
                        EnemyComponent enemy = newEnemy.GetComponent<EnemyComponent>();
                        if (enemy != null) {
                            enemy.Init(level, enemyInfo);
                        } else {
                            PeopleComponent people = newEnemy.GetComponent<PeopleComponent>();
                            if (people != null)
                            {
                                people.Init(level);
                            }
                            else
                            {
                                Debug.Log("Unknown enemy type");
                            }
                         }
                        Play(spawnClip);
                    }
                    enemiesLeft--;
                    heat = cooldown;
                }
                else
                {
                    heat -= Time.fixedDeltaTime;
                }
            } else
            {
                // Undeploy
                StartCoroutine(Undeploy());
            }
        }
	}

    public void Deploy()
    {
        StartCoroutine(DoDeploy());
    }

    IEnumerator DoDeploy()
    {
        if (!animInProgress)
        {
            animInProgress = true;
            yield return new WaitForSeconds(Random.Range(0.3f, 0.7f));
            Play(startClip);
            while (transform.localScale.magnitude < 0.98 * deployedScale.magnitude)
            {
                transform.localScale = (transform.localScale * 3 + deployedScale) / 4;
                yield return new WaitForSecondsRealtime(0.015f);
            }
            transform.localScale = deployedScale;
            yield return new WaitForSeconds(0.2f);

            while (Vector3.Distance(blades[0].position, transform.position) < 0.8f)
            {
                for (int i = 0; i < blades.Count; i++)
                {
                    Transform blade = blades[i];
                    blade.Rotate(0, 0, -2f);
                    blade.position += blade.rotation * Vector3.left * 0.02f;
                }
                yield return new WaitForSecondsRealtime(0.015f);
            }

            yield return new WaitForSeconds(delay);

            deployed = true;
            animInProgress = false;
        }
    }

    IEnumerator Undeploy()
    {
        if (!animInProgress)
        {
            animInProgress = true;
            yield return new WaitForSeconds(0.5f);
            Play(endClip);
            while (transform.localScale.magnitude > 0.02)
            {
                transform.localScale = (transform.localScale * 4 + Vector3.zero) / 5;
                yield return new WaitForSecondsRealtime(0.015f);
            }
            level.MarkObjectDead(gameObject);
            Destroy(GetComponent<SpriteRenderer>());
            yield return new WaitForSeconds(2f);
            Destroy(gameObject);
        }
    }

}
