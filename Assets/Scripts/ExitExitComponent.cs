﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitExitComponent : MonoBehaviour {

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.CompareTag("Player"))
        {
            Application.Quit();
        }
    }
}
