﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "LayoutDatabase", menuName = "Level Layout Database", order = 0)]
public class LevelLayoutDatabase : ScriptableObject
{
    public LevelLayout originLayout;

    public List<LevelLayout> layoutsLv1;

    public List<LevelLayout> layoutsLv2;

    public List<LevelLayout> layoutsLv3;
}
