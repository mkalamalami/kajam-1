﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ExitRestartComponent : MonoBehaviour {

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.CompareTag("Player"))
        {
            Destroy(GameObject.FindGameObjectWithTag("ExitMusic"));
            SceneManager.LoadScene("Game");
        }
    }
}
