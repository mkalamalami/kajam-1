﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitAlarmComponent : MonoBehaviour {

    private bool played = false;

    public void OnTriggerEnter2D(Collider2D collider)
    {
        if (!played && collider.tag == "Player")
        {
            played = true;
            GetComponent<AudioSource>().Play();
        }
    }
}
