﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MazeGenerator : MonoBehaviour {

    public static string HINT_ZERO = "0";

    public class Maze
    {
        public int iExit;

        public int jExit;

        public string[][] hints;

        public int size;

        public string GetHint(int i, int j)
        {
            if (i < 0 || j < 0 || i >= hints.Length || j >= hints.Length)
            {
                return null;
            } else
            {
                return hints[i][j];
            }
        }
    }
    
    private class Cell
    {
        public Cell(int i, int j, int gridSize)
        {
            this.i = i;
            this.j = j;
            this.gridSize = gridSize;
            pathId = j * gridSize + i;

            if (j == 0) t = false;
            if (i == gridSize - 1) r = false;
            if (j == gridSize - 1) b = false;
            if (i == 0) l = false;
        }

        public int gridSize;
        public int pathId;
        public int i;
        public int j;

        public bool t = true;
        public bool r = true;
        public bool b = true;
        public bool l = true;
    }

    public static Maze Generate(int size)
    {
        // Generate maze walls
        Cell[][] grid = CreateGrid(size);
        List<Cell> changeableCells = CreateChangeableCellList(grid);
        int i = 0;
        while (!IsMazeComplete(grid) && i++ < size * size)
        { 
            RemoveRandomWall(grid, changeableCells);
        }

        /*  if (i >= size * size)
          {
              Debug.Log("Gave up general");
          }
          Trace(grid);*/

        // Choose exit
        Vector2 exit = ChooseExit(size);
        Maze maze = new Maze();
        maze.iExit = (int) exit.x;
        maze.jExit = (int) exit.y;
        maze.size = size;

        // Compute hints
        maze.hints = ComputeHints(grid, maze);

        return maze;
    }

    private static Vector2 ChooseExit(int size)
    {
        int i = Random.Range(1, size - 1); // no corners
        int j = Random.Range(1, size - 1);
        if (i < j)
        {
            if (i + j < size)
            {
                i = 0;
            }
            else
            {
                j = size - 1;
            }

        }
        else
        {
            if (i + j < size)
            {
                j = 0;
            }
            else
            {
                i = size - 1;
            }
        }
        return new Vector2(i, j);
    }

    private static string[][] ComputeHints(Cell[][] grid, Maze maze)
    {
        // Init hints grid
        string[][] hints = new string[maze.size][];
        for (int i = 0; i < maze.size; i++)
        {
            hints[i] = new string[maze.size];
            for (int j = 0; j < maze.size; j++)
            {
                hints[i][j] = "";
            }
        }

        // Walk to all directions from exit
        List<Cell> nextCells = new List<Cell>();
        hints[maze.iExit][maze.jExit] = HINT_ZERO;
        nextCells.Add(grid[maze.iExit][maze.jExit]);
        int distance = 1;
        char variant;
        while (nextCells.Count > 0)
        {
            variant = 'A';
            List<Cell> unexploredCells = new List<Cell>();
            foreach (Cell cell in nextCells)
            {
                List<Cell> neighbors = new List<Cell>();
                if (!cell.t && cell.j > 0) neighbors.Add(grid[cell.i][cell.j - 1]);
                if (!cell.r && cell.i < maze.size - 1) neighbors.Add(grid[cell.i + 1][cell.j]);
                if (!cell.b && cell.j < maze.size - 1) neighbors.Add(grid[cell.i][cell.j + 1]);
                if (!cell.l && cell.i > 0) neighbors.Add(grid[cell.i - 1][cell.j]);

                foreach (Cell neighbor in neighbors) {
                    if (hints[neighbor.i][neighbor.j] == "")
                    {
                        hints[neighbor.i][neighbor.j] = distance.ToString() + variant;
                        unexploredCells.Add(neighbor);
                        variant++;
                    }
                }
            }
            distance++;
            nextCells = unexploredCells;
        }

        return hints;
    }

    private static Cell[][] CreateGrid(int size)
    { 
        Cell[][] grid = new Cell[size][];
        for (int i = 0; i < size; i++)
        {
            grid[i] = new Cell[size];
            for (int j = 0; j < size; j++)
            {
                grid[i][j] = new Cell(i, j, size);
            }
        }
        return grid;
    }

    private static List<Cell> CreateChangeableCellList(Cell[][] grid)
    {
        List<Cell> cells = new List<Cell>();
        foreach (Cell[] line in grid)
        {
            foreach (Cell cell in line)
            {
                cells.Add(cell);
            }
        }
        return cells;
    }

    private static void RemoveRandomWall(Cell[][] grid, List<Cell> changeableCells)
    {
        bool done = false;
        int cellPicks = 0;

        while (!done && cellPicks++ < 10) {

            Cell cell = changeableCells[Random.Range(0, changeableCells.Count)];
            int direction = Random.Range(0, 4);
            int attempts = 0;

            while (!done && attempts++ < 4)
            {
                switch (direction)
                {
                    case 0:
                        if (cell.t)
                        {
                            Cell topCell = grid[cell.i][cell.j - 1];
                            if (topCell.pathId != cell.pathId)
                            {
                                cell.t = false;
                                topCell.b = false;
                                MergePaths(grid, cell.pathId, topCell.pathId);
                                done = true;
                            }
                        }
                        break;
                    case 1:
                        if (cell.r)
                        {
                            Cell rightCell = grid[cell.i + 1][cell.j];
                            if (rightCell.pathId != cell.pathId)
                            {
                                cell.r = false;
                                rightCell.l = false;
                                MergePaths(grid, cell.pathId, rightCell.pathId);
                                done = true;
                            }
                        }
                        break;
                    case 2:
                        if (cell.b)
                        {
                            Cell bottomCell = grid[cell.i][cell.j + 1];
                            if (bottomCell.pathId != cell.pathId)
                            {
                                cell.b = false;
                                bottomCell.t = false;
                                MergePaths(grid, cell.pathId, bottomCell.pathId);
                                done = true;
                            }
                        }
                        break;
                    case 3:
                        if (cell.l)
                        {
                            Cell leftCell = grid[cell.i - 1][cell.j];
                            if (leftCell.pathId != cell.pathId)
                            {
                                cell.l = false;
                                leftCell.r = false;
                                MergePaths(grid, cell.pathId, leftCell.pathId);
                                done = true;
                            }
                        }
                        break;
                }

                if (!done)
                {
                    direction = (direction + 1) % 4;
                }
            }

            if (!done)
            {
                changeableCells.Remove(cell);
              //  Debug.Log("Cell " + cell.i + ";" + cell.j + " over");
            }
        }
    }

    private static void MergePaths(Cell[][] grid, int pathId1, int pathId2)
    {
        int toPathId = Mathf.Min(pathId1, pathId2);
        foreach (Cell[] line in grid)
        {
            foreach (Cell cell in line)
            {
                if (cell.pathId == pathId1 || cell.pathId == pathId2)
                {
                    cell.pathId = toPathId;
                }
            }
        }
    }

    private static bool IsMazeComplete(Cell[][] grid)
    {
        foreach (Cell[] line in grid)
        {
            foreach (Cell cell in line)
            {
                if (cell.pathId != 0)
                {
                    return false;
                }
            }
        }
      //  Debug.Log("Complete!");
        return true;
    }

    private static void Trace(Cell[][] grid)
    {
        Debug.Log("Results");
        string[] trace = new string[3 * (grid.Length + 1)];
        for (int i = 0; i < trace.Length; i++)
        {
            trace[i] = "";
        }

        for (int i = 0; i < grid.Length; i++)
        {
            for (int j = 0; j < grid.Length; j++)
            {
                Cell cell = grid[i][j];
                trace[cell.j * 3] += cell.t ? " -- " : "    ";
                trace[cell.j * 3 + 1] +=
                        (cell.l ? "|" : " ")
                        + (cell.pathId < 10 ? " " : "") + cell.pathId +
                        (cell.r ? "|" : " ");
                trace[cell.j * 3 + 2] += cell.b ? " -- " : "    ";
            }
        }

        foreach (string line in trace)
        {
            Debug.Log(line);
        }
    }
    
}
