﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundComponent : MonoBehaviour {

    public Transform target;

    public float ratio = 20.5f;

    private Material material;

    void Start ()
    {
        material = GetComponent<SpriteRenderer>().material;
    }

	void Update () {
       material.mainTextureOffset = new Vector2(-transform.position.x - target.position.x,
           - transform.position.y - target.position.y) / ratio;
	}
}
