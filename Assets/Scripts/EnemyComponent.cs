﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyComponent : AbstractComponent {

    public const int KILLED_ENEMIES_LAYER = 8;

    private const float DEFAULT_SPEED = 2.0f;

    private const float DEFAULT_ROTATION_SPEED = 0.07f;

    public ParticleSystem explosionSmall;

    public ParticleSystem explosionBig;

    public List<AudioClip> hitClips;

    public List<AudioClip> walkClips;

    public AudioClip deathClip;

    public List<SpriteRenderer> sprites;

    public EnemyPartComponent enemyPartPrefab;

    private Transform target;

    private Rigidbody2D r;

    private int health = 2;

    private bool isDead = false;

    private LevelComponent level;

    private AudioClip ownWalkClip;

    private float speed = DEFAULT_SPEED;

    private float rotationSpeed = DEFAULT_ROTATION_SPEED;

    private float rotationAngle = 0;

    private float rotationAngleLastChecked = 0;

    public void Init(LevelComponent level, EnemyInfo enemy)
    {
        this.level = level;
        level.AddAliveObject(gameObject);

        foreach (SpriteRenderer sprite in sprites)
        {
            if (sprite.name.Equals("Body"))
            {
                sprite.sprite = enemy.bodySprite;
            }
            else
            {
                sprite.sprite = enemy.legSprite;
            }
        }
        transform.localScale = new Vector3(transform.localScale.x * enemy.scale, transform.localScale.y * enemy.scale, 1);
        r = GetComponent<Rigidbody2D>();
        r.mass *= enemy.scale;
        health = enemy.health;
        speed = DEFAULT_SPEED * enemy.speed;
        rotationSpeed = DEFAULT_ROTATION_SPEED * enemy.speed;
    }

    void Start ()
    {
        ownWalkClip = walkClips[Random.Range(0, walkClips.Count)];

        PlayerComponent player = FindObjectOfType<PlayerComponent>();
        if (player != null)
        {
            target = FindObjectOfType<PlayerComponent>().transform;
        }

        r.rotation = Random.Range(0, 360);
        StartCoroutine(AnimateEntry());

        if (level == null)
        {
            Debug.LogError("Level not set on enemy");
        }
    }

    IEnumerator AnimateEntry()
    {
        Color c = new Color(0.1f, 0.1f, 0.1f);
        float alpha = 1;
        while (alpha > 0)
        {
            foreach (SpriteRenderer sprite in sprites) Flash(sprite, c, alpha);
            alpha -= 0.03f;
            yield return new WaitForSecondsRealtime(0.018f);
        }
    }


    void FixedUpdate() {
        if (!isDead && target && !target.GetComponent<PlayerComponent>().isDead)
        {
            // Compute angle towards player
            float playerAngle = Vector2.SignedAngle(Vector2.up, target.position - transform.position);
            float deltaAngle = playerAngle - r.rotation;
            if (deltaAngle > 180) deltaAngle -= 360;
            if (deltaAngle < -180) deltaAngle += 360;

            // Rotate towards player
            if (Time.fixedTime - rotationAngleLastChecked > 0.2f)
            {
                if (Mathf.Abs(deltaAngle) > 180)
                {
                    if (playerAngle > r.rotation) r.rotation += 360;
                    else r.rotation -= 360;
                }
                rotationAngle = deltaAngle * rotationSpeed;
                rotationAngleLastChecked = Time.fixedTime;
            }
            r.rotation += rotationAngle;

            // Move forward, slower if we're not in the right direction
            Vector2 direction = Quaternion.AngleAxis(r.rotation, Vector3.forward) * Vector2.up;
            r.velocity = direction * speed * (deltaAngle > 30 ? 0.3f : 1);

            // Clamp angular velocity (e.g. after getting shot)
            r.angularVelocity = Mathf.Sign(r.angularVelocity) * Mathf.Min(Mathf.Sign(r.angularVelocity), 1000);

            // Walk sounds
            AudioSource source = GetComponent<AudioSource>();
            if (!source.isPlaying)
            {
                Play(source, ownWalkClip, 0.2f, 0.1f);
                source.volume *= 0.3f;
                source.loop = true;
            }
        }
        else {
            r.velocity *= 0.6f;
        }
	}

    public void Hit()
    {
        StartCoroutine(DoHit());
    }

    IEnumerator DoHit()
    {
        if (!isDead)
        {
            health--;
            AudioSource source = GetComponent<AudioSource>();
            CameraComponent.FreezeTimeForAMoment();
            if (health <= 0)
            {
                isDead = true;
                Play(source, deathClip, 0.2f, 0.1f);
                source.volume -= 0.2f;
                foreach (SpriteRenderer sprite in sprites) Flash(sprite, Color.white, 1);
                yield return DoKill();
            } else
            {
                SpawnEnemyPart();
                Play(source, hitClips[Random.Range(0, hitClips.Count)], 0.2f, 0.1f);
                explosionSmall.Play();
                foreach (SpriteRenderer sprite in sprites) Flash(sprite, Color.white, 1);
                yield return new WaitForSeconds(0.05f);
                foreach (SpriteRenderer sprite in sprites) Flash(sprite, Color.white, 0);
            }
        }
    }


    IEnumerator DoKill()
    {
        for (int i = 0; i < 3; i++)
        {
            SpawnEnemyPart();
        }

        gameObject.layer = KILLED_ENEMIES_LAYER;
        explosionBig.Play();

        Color color = GetComponent<SpriteRenderer>().color;
        GetComponent<SpriteRenderer>().color = new Color(color.r, color.g, color.b, 0.3f);
        FindObjectOfType<CameraComponent>().Shake(0.3f);

        yield return new WaitForSeconds(0.2f);
        Destroy(GetComponent<CircleCollider2D>());
        while (transform.localScale.x > 0.02f)
        {
            transform.localScale = Vector3.Scale(transform.localScale, new Vector3(0.8f, 0.8f, 1f));
            yield return new WaitForSecondsRealtime(0.018f);
        }
        Destroy(GetComponent<SpriteRenderer>());
        yield return new WaitForSeconds(0.5f);
        Destroy(gameObject);
        if (level != null)
        {
            level.MarkObjectDead(gameObject);
        }
    }

    void SpawnEnemyPart()
    {
        GameObject enemyPart = Instantiate(enemyPartPrefab.gameObject, null);
        enemyPart.GetComponent<EnemyPartComponent>().Init(transform.position);
    }
}
